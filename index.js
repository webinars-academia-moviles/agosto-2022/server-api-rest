const express = require("express");
const creditCard = require("./routes/creditCard.js");

const app = express();

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "OPTIONS, GET, POST, PUT, PATCH, DELETE"
  );
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
  if (req.method === "OPTIONS") {
    return res.sendStatus(200);
  }
  next();
});

app.use("/creditCard/api", creditCard);

console.log("\n", "\x1b[35m", "****** API REST *****", "\n");

console.log("\x1b[36m%s\x1b[0m", "http://localhost:4000/creditCard/api/bfree");
console.log("\x1b[36m%s\x1b[0m", "http://localhost:4000/creditCard/api/platinium");
console.log("\x1b[36m%s\x1b[0m", "http://localhost:4000/creditCard/api/black");
console.log("\x1b[36m%s\x1b[0m", "http://localhost:4000/creditCard/api/gold");

app.listen(4000);
