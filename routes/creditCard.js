const express = require("express");
const creditCardController = require("../controllers/creditCardController");

const router = express.Router();

router.route("/bfree").get(creditCardController.bfree);
router.route("/platinium").get(creditCardController.platinium);
router.route("/black").get(creditCardController.black);
router.route("/gold").get(creditCardController.gold);

module.exports = router;
