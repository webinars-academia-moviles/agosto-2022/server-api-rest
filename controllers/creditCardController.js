exports.bfree = (req, res, next) => {
  setTimeout(() => {
    return res.status(200).json({
      status: "success",
      data: {
        cardName: "Bfree",
        last4Number: "1234",
        iconName: "bfree",
        availableLine: "S/ 6,000.00",
        creditLine: "S/ 6,000.00",
      },
    });
  }, 3000);
};
exports.platinium = (req, res, next) => {
  setTimeout(() => {
    return res.status(200).json({
      status: "success",
      data: {
        cardName: "Platinium",
        last4Number: "5678",
        iconName: "platinium",
        availableLine: "S/ 8,000.00",
        creditLine: "S/ 10,000.00",
      },
    });
  }, 4000);
};
exports.black = (req, res, next) => {
  setTimeout(() => {
    return res.status(200).json({
      status: "success",
      data: {
        cardName: "Black",
        last4Number: "1234",
        iconName: "black",
        availableLine: "S/ 28,000.00",
        creditLine: "S/ 30,000.00",
      },
    });
  }, 2000);
};

exports.gold = (req, res, next) => {
  setTimeout(() => {
    return res.status(404).json({ message: "no se encontró información" });
  }, 2500);
};
